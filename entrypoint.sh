#!/bin/bash

for d2 in $(pwd)/*.d2; do
    diagram=$(basename $d2 .d2)
    d2 $diagram.d2 /d2out/$diagram.svg
done
