# D2 TALA

Docker image to be used in the CI pipelines to render D2 diagram repositories. Includes the TALA layout engine.

## Versions

These versions have been tested working as expected

```yaml
D2_VERSION: v0.6.3
TALA_VERSION: v0.3.12
```

## Usage

Running the image renders all `.d2` files under the mounted `/d2` directory out into `/d2out`

Diagrams can be excluded from being rendered by prefixing their name by `.`. This is useful for includes. In this repo as an example `.render-config.d2`

## Notes

Example diagram in this repository is a direct copy of the example listed on the main page of the D2 engine, but split up to showcase render exclusion.

<https://d2lang.com/>
