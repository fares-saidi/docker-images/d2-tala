FROM "golang"

ARG D2_VERSION=v0.6.3
ARG TALA_VERSION=v0.3.12

# Install d2
# RUN go install oss.terrastruct.com/d2@$D2_VERSION
# Install TALA layout engine
RUN curl -fsSL https://d2lang.com/install.sh | sh -s -- --version $D2_VERSION --tala $TALA_VERSION

RUN mkdir /d2
RUN mkdir /d2out

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /d2

ENTRYPOINT [ "/entrypoint.sh" ]
